/************************************************************************
**
** Copyright © 2014 Antivirus4OS. All rights reserved.
** Contacts: av4os.sentinel@gmail.com
**
** This file is part of the Sentinel project.
** You may use this file under the terms of the Expat license as follows:
**
** "Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
**
*************************************************************************/
#ifndef CHECKFILE_H
#define CHECKFILE_H

#include <QString>
#include <QFile>
#include <QIODevice>
#include <QMessageBox>
#include <QByteArray>
#include <QList>
#include <QTableWidget>

#include "functor.h"

// format of result of checking file
struct ResultOfCheckingFile{
    QString m_date;
    QString m_time;
    QString m_path;
    QVector<QByteArray> m_viruses;
    ResultOfCheckingFile(const QString& date,const QString& time, const QString path, const QVector<QByteArray> viruses):
                            m_date(date) , m_time(time) , m_path(path) , m_viruses(viruses){

    }
    QString toQString();            //convert vector of viruses to QString
};

class fileChecker: public functor
{
private:
    QList<ResultOfCheckingFile> listOfCheckedFiles;
    QFile base;

public:
    fileChecker(): base("data/virusbase")
    {
        if (!base.open(QIODevice::ReadOnly))
        {
            QMessageBox msgBox;
            msgBox.setText("Base is not opened");
            msgBox.exec();
            throw 1;
        }
    }

    void operator()(const QString& fileName);
    QList<ResultOfCheckingFile> getList();
    void pushToList(const ResultOfCheckingFile & result);
    ~fileChecker()
    {
        base.close();
    }
};

void checkFile(const QString & );

QList<ResultOfCheckingFile> checkDir(const QString & , bool onlyExe = false);   // check the chosen directory

#endif // CHECKFILE_H
