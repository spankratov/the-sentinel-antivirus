/************************************************************************
**
** Copyright © 2014 Antivirus4OS. All rights reserved.
** Contacts: av4os.sentinel@gmail.com
**
** This file is part of the Sentinel project.
** You may use this file under the terms of the Expat license as follows:
**
** "Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
**
*************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ch.h"
#include "dirwidget.h"
#include"avwindow.h"
#include "hashwindow.h"
#include <QPropertyAnimation>
#include "setwin.h"
#include "aboutus.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:
//    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *event);
    void resizeEventAV(QResizeEvent *event);
    void resizeEventH(QResizeEvent *event);
    void resizeEventSW(QResizeEvent *event);
    void resizeEventA(QResizeEvent *event);
   // void mouseMoveEvent(QMouseEvent *ev);
private slots:
    /*void on_GetHashButton_clicked();

    void on_checkFileButton_clicked();

    void on_pushButtonCheckDir_clicked();
    */
    void slotShowHideSlide();
    void slotShowHideSlideAV();
    void slotShowHideSlideH();
    void slotShowHideSlideSW();
    void slotShowHideSlideA();



private:
    Ui::MainWindow *ui;
    //Ch *ChoseDirectoryPls;
    DirWidget *m_slideWIdget;
    AVWindow *m_slideWIdgetAV;
    HashWindow *m_slideWIdgetH;
    SetWin *m_slideWIdgetSW;
    AboutUs *m_slideWIdgetA;
    //Ch *m_slideWIdget;
// // Directory slide
    QPropertyAnimation *m_animation;
    QRect m_geometry;
    bool m_boolHide;
    void showHideSlideWidget(bool f_flag);
    // AV slide
    QPropertyAnimation *m_animationAV;
    QRect m_geometryAV;
    bool m_boolHideAV;
    void showHideSlideWidgetAV(bool f_flag);
    // Hash slide
    QPropertyAnimation *m_animationH;
    QRect m_geometryH;
    bool m_boolHideH;
    void showHideSlideWidgetH(bool f_flag);
    // Settings slide
    QPropertyAnimation *m_animationSW;
    QRect m_geometrySW;
    bool m_boolHideSW;
    void showHideSlideWidgetSW(bool f_flag);
    // About page
    QPropertyAnimation *m_animationA;
    QRect m_geometryA;
    bool m_boolHideA;
    void showHideSlideWidgetA(bool f_flag);

};

#endif // MAINWINDOW_H
