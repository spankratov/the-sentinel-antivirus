/************************************************************************
**
** Copyright © 2014 Antivirus4OS. All rights reserved.
** Contacts: av4os.sentinel@gmail.com
**
** This file is part of the Sentinel project.
** You may use this file under the terms of the Expat license as follows:
**
** "Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
**
*************************************************************************/
#include "hashwindow.h"
#include "ui_hashwindow.h"
#include "GetHash.h"
#include "Lang.h"
#ifdef Q_OS_WIN
#include <windows.h>
#include <shellapi.h>
#endif

HashWindow::HashWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HashWindow)
{
    ui->setupUi(this);
    CheckOS();
}

HashWindow::~HashWindow()
{
    delete ui;
}

void HashWindow::on_HashCheck_clicked()
{
    //ToDo: test on big files
    QString FileDir = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.*)"));
    QString hash = GetHash(FileDir);
    ui->pathFolder->setText(FileDir);


    if(hash.isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error!");
        msgBox.setText("Can not get the hash of the file.");
        msgBox.exec();
    }
    else
    {
        ui->HashSumm->setText(hash);

    }

}

void HashWindow::CheckOS()
{
    ui->SystemDirCheck->hide();
    ui->label_2->hide();
    #ifdef Q_OS_WIN
    ui->SystemDirCheck->show();
     ui->label_2->show();
    #endif

}

void HashWindow::on_SystemDirCheck_clicked()
{

ShellExecute(0, L"runas", L"cmd.exe", L"/c sfc /scannow & pause", 0, SW_SHOWNORMAL);

}
