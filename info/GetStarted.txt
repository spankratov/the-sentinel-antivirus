
- GETTING STARTED WITH SENTINEL -

Welcome and thank you for downloading our anti-virus program.
We appreciate it that you chose us and would like to make 
your experience with our software as comfortable as possible, 
so feel free to ask us any questions concerning Sentinel 
at <av4os.sentinel@gmail.com>!

Downloading & Installation
---------------------------

Please see the file «INSTALL» for instructions.

Getting Acquainted
-------------------

Sentinel is a free open-source cross-platform antivirus 
program developed by Antivirus4OS, students of 
Saint Petersburg State University.

Features
---------

So far Sentinel has 4 features, such as:

 - Hash;
 - Antivirus;
 - Directory Check;
 - System directory check.

«Hash» allows you to calculate a checksum for a file by MD5 algorithm.
«Antivirus» lets you check chosen file for viruses from its database.
«Directory Check» scans chosen directory for viruses from its database.
So does the «System directory check».

As you may see, last two use «database» to find viruses. This database is included in project under «../data/virusbase». To the present moment, Sentinel uses its own database composed of bits of code used by viruses. For now, this database is a mere example of what the program can do. Wait for more releases to find out!

Обновления
-----------

Before each use of Sentinel we advise you to update your virus database by launching the «Update.jar» file in the «data» folder. Note that you will need JVM (java virtual machine) installed for the correct work of this file.

----------

Copyright © 2014 Antivirus4OS. This file is part of the Sentinel. All rights reserved. Contact us: <av4os.sentinel@gmail.com>
