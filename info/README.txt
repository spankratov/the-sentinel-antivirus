SENTINEL

Last updated for Sentinel 1.0 on 2014-12-18

The project home page is: http://sentinel.16mb.com/
BitBucket repository: https://bitbucket.org/kirovverst/antivirusforos

-----------------------------------------------------------------------------
What is it?

Sentinel is a free open-source cross-platform antivirus program developed by Antivirus4OS, students of the Saint Petersburg State University, for the scientific project.

Sentinel was created using the QT toolkit, to be cross-platform, and is licensed under the Expat license. 

This is a source release. With appropriate development tools on a compatible platform, you can build this on your system.

-----------------------------------------------------------------------------
The Latest Version

You can find details of the latest version in a file "ChangeLog" in the "INFO" folder.

-----------------------------------------------------------------------------
Requirements

This release has been successfully built on Windows 7, Windows 8 and Windows 8.1 with the Qt v5.3 and JVM (java virtual machine).
It may work on previous versions, however it is yet to be confirmed.

-----------------------------------------------------------------------------
Download

You can obtain your copy of Sentinel in these ways:

1. If you wish to get SOURCE FILES, please download our full repository in the "Download" tab.
2. If you wish to get PROGRAM, please download our Setup master or the source .zip file in the "Download" tab.

-----------------------------------------------------------------------------
Documentation

Documentation for Sentinel is available in the "INFO" folder, as well as the requirements for building it on your system followed with installation instructions (please see the "INSTALL" file) and step-by-step guide. 

Documentation is also available in Russian version. Please see the appropriate download file in the "Downloads" tab. It also comes with Russified README file.

-----------------------------------------------------------------------------
Licensing

Please see the file called "LICENSE".

-----------------------------------------------------------------------------
Contacts

If you have a concrete bug report, feature request, want to join the development team or get announcements, please contact us at <av4os.sentinel@gmail.com>

-----------------------------------------------------------------------------
Developers

List of developers working on this project:
Azat Abubakirov, Stanislav Pankratov, Victor Punin, Alexey Malyutin, Alexander Gnatuyk, Dmitry Grachev, Veronica Borozyonnaya.

-------------------------------------------------------------------------------
Copyright © 2014 Antivirus4OS.
This file is part of the Sentinel. All rights reserved.