/************************************************************************
**
** Copyright © 2014 Antivirus4OS. All rights reserved.
** Contacts: av4os.sentinel@gmail.com
**
** This file is part of the Sentinel project.
** You may use this file under the terms of the Expat license as follows:
**
** "Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
**
*************************************************************************/
#include "dirwidget.h"
#include "ui_dirwidget.h"
#include "QFile"
#include "QFileDialog"
#include <QDebug>
#include "mainwindow.h"

DirWidget::DirWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DirWidget)
{
    ui->setupUi(this);
}

DirWidget::~DirWidget()
{
    delete ui;
}
void DirWidget::on_ChosenDirectory_clicked()
{
     QString ChosenDir = QFileDialog::getExistingDirectory(0, "Open folder", "");
     ui->pathFolder->setText(ChosenDir);
}

void DirWidget::on_Ok_clicked()
{
    try
    {
        QString path = ui->pathFolder->text();
        if(path.isEmpty())
        {
            QMessageBox::warning(this, "Warning!", "Please specify the directory!");
            return;
        }

        ui->Ok->hide();

        QList<ResultOfCheckingFile> results;


        if(ui->onlyExe->isChecked()) {
            results = checkDir(path,true);              // check only executable files
        }else{
            results = checkDir(path);                   // check all files
        }

        ui->Ok->show();

        if(ui->onlyVirus->isChecked()){

            for (QList<ResultOfCheckingFile>::const_iterator it = results.constBegin(); it != results.constEnd(); ++it){
                if (!it->m_viruses.isEmpty()){

                    ResultOfCheckingFile var = *it;
                    addNewRow(var);
                }
            }
        }else{
            foreach (ResultOfCheckingFile result, results) {
                addNewRow(result);
            }
        }

        ui->countOfCheckedFiles->setText(QString::number(results.size()));
    }
    catch(const QString& error)
    {
        QMessageBox::critical(this, "Error!", error);
        close();
    }
    catch(...)
    {
        QMessageBox::critical(this, "Error!", "Unknown error has ocurred.");
        close();
    }
}


void DirWidget::addNewRow(ResultOfCheckingFile & result){

    ui->virusTable->insertRow(0);                   // add new row of table
    ui->virusTable->setItem(0,0,new QTableWidgetItem(result.m_date));     // set date of checking
    ui->virusTable->setItem(0,1,new QTableWidgetItem(result.m_time));     // set time of checking
    ui->virusTable->setItem(0,2,new QTableWidgetItem(result.m_path));     // set path of the checked file

    if (result.m_viruses.isEmpty()){
        ui->virusTable->setItem(0,3,new QTableWidgetItem(NOT_INFECTED));  // set infected file or not
        ui->virusTable->setItem(0,4,new QTableWidgetItem("Not found"));
    }else{
        ui->virusTable->setItem(0,3,new QTableWidgetItem(INFECTED));
        QString foundViruses = result.toQString();
        ui->virusTable->setItem(0,4,new QTableWidgetItem(foundViruses));
    }
}

void DirWidget::on_cancel_clicked()
{
    // Attention! Куча костылей, значительно лучше сделать просто delete this, но тогда нужно немного менять код в mainwindow

    ui->virusTable->clearContents();
    ui->pathFolder->clear();
    ui->countOfCheckedFiles->clear();
    ui->timer->clear();
    // + добавить сброс чекбоксов.


    close();
    // MainWindow::ui->pushButtonCheckDir->setIcon(QIcon("4GUI/CheckDirOff.png")); public для ui не помог... :(


    // + неплохо бы заменить на slotShowHideSlide() из класса mainwindow... Уже и public делал, а все равно ругается...
}
//  background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(249, 249, 249, 0), stop:1 rgba(249, 249, 249, 0));

