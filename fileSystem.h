/************************************************************************
**
** Copyright © 2014 Antivirus4OS. All rights reserved.
** Contacts: av4os.sentinel@gmail.com
**
** This file is part of the Sentinel project.
** You may use this file under the terms of the Expat license as follows:
**
** "Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
**
*************************************************************************/
#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <QFileInfo>
#include <QDir>

#include "functor.h"

// each navigating functions accept:
// 1) directory (directories) that will be browsed recursively
// 2) functor that will be called on each file with directory
// 3) filters that determine which files will be browsed; by default: only executables
// 4) browsing order; by default: directories first, then files

void browseFileSystem(const QFileInfoList& directories, functor& checkFunction, QDir::Filters filters =
        QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs | QDir::Executable,
                    QDir::SortFlags sort = QDir::DirsFirst);

// special function for many directories to check
// typically it will be called when it is necessary to check all the file system:
// checkDirectories(QDir::drives(), checkFunction) (should work on all operating systems)

void browseFileSystem(const QString& directoryName, functor& checkFunction, QDir::Filters filters =
        QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs | QDir::Executable,
                    QDir::SortFlags sort = QDir::DirsFirst);


#endif // FILESYSTEM_H
