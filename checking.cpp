/************************************************************************
**
** Copyright © 2014 Antivirus4OS. All rights reserved.
** Contacts: av4os.sentinel@gmail.com
**
** This file is part of the Sentinel project.
** You may use this file under the terms of the Expat license as follows:
**
** "Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
**
*************************************************************************/
#include <QDateTime>

#include "checking.h"
#include "fileSystem.h"

void fileChecker::operator()(const QString& fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)){
        QMessageBox msgBox;
        msgBox.setText("File is not opened");
        msgBox.exec();
        return;
    }

    QByteArray dataFile = file.readAll();       //get all data from checking file

    QVector<QByteArray> foundViruses;

    while(!base.atEnd()){
        QByteArray signature = base.readLine();

        if (signature[signature.size() - 1] == '\n'){
             signature.remove(signature.size() - 1, 1);
        }
        if (foundViruses.indexOf(signature) != -1){     // if the current signature is already pushed
             continue;
        }

        if (dataFile.indexOf(signature) != -1){         // if the current signature is found in the file
            foundViruses.push_back(signature);
        }
    }

    QString date = QDateTime::currentDateTime().toString("dd.MM.yyyy");
    QString time = QDateTime::currentDateTime().toString("hh:mm:ss");

    ResultOfCheckingFile result(date,time,fileName,foundViruses);

    pushToList(result);             //      push the result of checking to the list of checked files

    file.close();
}


void fileChecker::pushToList(const ResultOfCheckingFile & result){
    listOfCheckedFiles.append(result);
}


QList<ResultOfCheckingFile> fileChecker::getList(){
    return listOfCheckedFiles;
}


QList<ResultOfCheckingFile> checkDir(const QString & FolderDir, bool onlyExe){
    fileChecker checker;


    if (onlyExe){
        browseFileSystem(FolderDir,checker);
    }else{
        browseFileSystem(FolderDir,checker, QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs);
    }
    return checker.getList();
}


QString ResultOfCheckingFile::toQString(){          // convert QByteArray to QString
    QString res = "";
    foreach (QByteArray virus, m_viruses) {
        res += (" " + virus);
    }
    return res;
}


